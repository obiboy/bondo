import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListeDonsPage } from './liste-dons.page';

describe('ListeDonsPage', () => {
  let component: ListeDonsPage;
  let fixture: ComponentFixture<ListeDonsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListeDonsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
