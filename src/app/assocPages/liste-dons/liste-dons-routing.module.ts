import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeDonsPage } from './liste-dons.page';

const routes: Routes = [
  {
    path: '',
    component: ListeDonsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeDonsPageRoutingModule {}
