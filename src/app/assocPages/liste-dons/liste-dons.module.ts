import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeDonsPageRoutingModule } from './liste-dons-routing.module';

import { ListeDonsPage } from './liste-dons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeDonsPageRoutingModule
  ],
  declarations: [ListeDonsPage]
})
export class ListeDonsPageModule {}
