import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeProjetPage } from './liste-projet.page';

const routes: Routes = [
  {
    path: '',
    component: ListeProjetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeProjetPageRoutingModule {}
