import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListeProjetPage } from './liste-projet.page';

describe('ListeProjetPage', () => {
  let component: ListeProjetPage;
  let fixture: ComponentFixture<ListeProjetPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListeProjetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
