import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeProjetPageRoutingModule } from './liste-projet-routing.module';

import { ListeProjetPage } from './liste-projet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeProjetPageRoutingModule
  ],
  declarations: [ListeProjetPage]
})
export class ListeProjetPageModule {}
