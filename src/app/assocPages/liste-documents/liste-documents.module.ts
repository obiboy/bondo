import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListeDocumentsPageRoutingModule } from './liste-documents-routing.module';

import { ListeDocumentsPage } from './liste-documents.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeDocumentsPageRoutingModule
  ],
  declarations: [ListeDocumentsPage]
})
export class ListeDocumentsPageModule {}
