import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListeDocumentsPage } from './liste-documents.page';

describe('ListeDocumentsPage', () => {
  let component: ListeDocumentsPage;
  let fixture: ComponentFixture<ListeDocumentsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListeDocumentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
