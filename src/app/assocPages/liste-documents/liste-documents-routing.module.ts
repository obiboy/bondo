import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeDocumentsPage } from './liste-documents.page';

const routes: Routes = [
  {
    path: '',
    component: ListeDocumentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeDocumentsPageRoutingModule {}
