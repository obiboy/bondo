import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListeNotificationPage } from './liste-notification.page';

describe('ListeNotificationPage', () => {
  let component: ListeNotificationPage;
  let fixture: ComponentFixture<ListeNotificationPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListeNotificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
