import { Injectable } from '@angular/core';
import { User } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class RealtimeDatabaseService {
 
  claimer=true

  constructor(private db: AngularFireDatabase,
    private toastController:ToastController
    ) {}
  async saveUser(user: string, userData: any):Promise<any> {
    try {
      await this.db.object(`users/${user}`).set(userData).then(result=>{ this.claimer=true
        console.log(this.claimer)
        this.presentToast('Enregistrer','success')
      })
      return this.claimer
      // await this.db.list(`users/${user}`).push(userData);
    } catch (error) {
      console.log(error);
    }
  }
  saveUserData(uid: any, datas: {}[]) {
    throw new Error('Method not implemented.');
  }
  async presentToast(messages: string, colors: string) {
    const toast = await this.toastController.create({
      message: messages,
      duration: 2000,
      color: colors,
      // position: position,
    });

    await toast.present();
  }
}
