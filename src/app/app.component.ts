import { register } from 'swiper/element/bundle';
import { Component, inject } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
register();
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  title = 'app works!';
  firestore: Firestore = inject(Firestore);
  constructor() {
    console.log(this.firestore);
  }
}
