import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ActionSheetController, AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { AuthService } from '../service/authService';
import { UserService } from '../service/user.service'; 


@Component({
  selector: 'app-testelog',
  templateUrl: './testelog.page.html',
  styleUrls: ['./testelog.page.scss'],
})
export class TestelogPage implements OnInit {
 
  staySignedIn: boolean = true;
  monFormulaire: FormGroup;
  user:any
  
  constructor(
    private actionSheetCtrl: ActionSheetController,
    private formBuilder: FormBuilder,
    public navCont: NavController,
    private alertController: AlertController,
		private router: Router,
		private authService: AuthService,
    private toastController: ToastController,
    public userService :UserService

  ) {
    this.monFormulaire = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

/////////password recovery///////////
  async presentAlert() {    const alert = await this.alertController.create({
      header: 'Entrez votre email',
      message: 'Un mail vous sera evoyer',
      backdropDismiss: false,
      inputs: [
        {
          name: 'emailR',
          type: 'email',
          placeholder: 'Email',
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Opération annulée');
          },
        },
        {
          text: 'OK',
          handler: (data) => {
           try{const joe= this.authService.PasswordRecover(data.emailR)}catch(e){console}           
            // Utilisez les valeurs des champs de saisie ici
            console.log('Valeur du champ de saisie 1 :', data.emailR);
          },
        },
      ],
    });

    await alert.present();
  }
/////End password recovery///////////

//////////phone login////////
  async logByPhon() {
    this.navCont.navigateForward('/login');
  }
/////End phone login////////

  
  ngOnInit() {

  }

  goToInscription() {
    this.navCont.navigateForward('/inscription');
    console.log('this is the name : ');
  }

  /////////login////////
	async login() {
		 this.user = await this.authService.logIn(this.monFormulaire.value.email,this.monFormulaire.value.password)
		if (this.user) {
			console.log('oui')
			console.log(this.user)
      this.router.navigateByUrl('/liste-associations');
      // this.router.navigateByUrl('/liste-associations', { state: this.user });
			// this.router.navigateByUrl('/home', { replaceUrl: true });
		} else {
      this.presentToast('Email ou mot de passe incorrect!')
			// this.showAlert('Login failed', 'Please try again!');
		}
	 }
  //////End login////////

  ////////logWithGo/////
 async logWithGo() {
   
    this.user = await this.authService.loginWithGoogle()
    
    if (this.user) {
      console.log('oui');
      //  this.userService.setUser(this.user.uid);
      console.log('this.userService.user');
      console.log(this.user.uid);
      console.log('/////////////////////////////////')
      await this.userService.checkProfile(this.user.uid,'User')
     console.log('RETURN.userService.user.uid');
    //  console.log(this.userService.getUser());
    this.router.navigateByUrl('/liste-associations',);
     // this.router.navigateByUrl('/home', { replaceUrl: true });
    // this.router.navigate(['liste-associations'],{ state: {usex:this.user} })
    } else {
     console.log('Login failed', 'Please try again!');
    //  this.userService.setUser(3444444444);
    //  console.log('this.userService.user');
    //  console.log(this.userService.user);
    //  console.log('RETURN.userService.user.uid');
    //  console.log(this.userService.getUser());
    // this.router.navigateByUrl('/liste-associations',);

    //  this.navCont.navigateForward('liste-associations');

     
   }
 }
//////End logWithGo/////

/////Action sheet////
async presentActionSheet() {
  const actionSheet = await this.actionSheetCtrl.create({
    header: 'Actions',
    buttons: [
      {
        text: 'Delete',
        role: 'destructive',
        data: {
          action: 'delete',
        },
      },
      {
        text: 'Share',
        data: {
          action: 'share',
        },
      },
      {
        text: 'Cancel',
        role: 'cancel',
        data: {
          action: 'cancel',
        },
      },
    ],
  });

  await actionSheet.present();
}

///End Action sheet//

//////toast message///
async presentToast(messages: string) {
  const toast = await this.toastController.create({
    message: messages,
    duration: 1500,
    // position: position,
  });

  await toast.present();
}
//////End toast message///
  async showAlert(header: string, message: string) {
		const alert = await this.alertController.create({
			header,
      
			message,
			buttons: ['OK']
		});
		await alert.present();
	}
}


