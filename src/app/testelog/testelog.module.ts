import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestelogPageRoutingModule } from './testelog-routing.module';

import { TestelogPage } from './testelog.page';
// import { UserService } from '../service/user.service';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    TestelogPageRoutingModule,
    ReactiveFormsModule 
  ],
  declarations: [TestelogPage],
  // providers: [UserService]
})
export class TestelogPageModule {}
