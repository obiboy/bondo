import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestelogPage } from './testelog.page';

describe('TestelogPage', () => {
  let component: TestelogPage;
  let fixture: ComponentFixture<TestelogPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TestelogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
