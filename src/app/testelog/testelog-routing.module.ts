import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestelogPage } from './testelog.page';

const routes: Routes = [
  {
    path: '',
    component: TestelogPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestelogPageRoutingModule {}
