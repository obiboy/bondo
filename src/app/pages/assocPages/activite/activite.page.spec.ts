import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivitePage } from './activite.page';

describe('ActivitePage', () => {
  let component: ActivitePage;
  let fixture: ComponentFixture<ActivitePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ActivitePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
