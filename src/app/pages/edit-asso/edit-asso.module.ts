import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditAssoPageRoutingModule } from './edit-asso-routing.module';

import { EditAssoPage } from './edit-asso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditAssoPageRoutingModule
  ],
  declarations: [EditAssoPage]
})
export class EditAssoPageModule {}
