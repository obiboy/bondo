import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditAssoPage } from './edit-asso.page';

describe('EditAssoPage', () => {
  let component: EditAssoPage;
  let fixture: ComponentFixture<EditAssoPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditAssoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
