import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditAssoPage } from './edit-asso.page';

const routes: Routes = [
  {
    path: '',
    component: EditAssoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditAssoPageRoutingModule {}
