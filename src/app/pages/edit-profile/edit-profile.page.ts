import { Component, OnInit } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import {
  FormBuilder,
  FormGroup,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { User } from 'firebase/auth';
import { doc, updateDoc } from 'firebase/firestore';
import { Observable, from, switchMap, tap } from 'rxjs';
import { ProfileUser } from 'src/app/model/user';
import { ImageUploadService } from 'src/app/service/image-upload.service';
import { UserService } from 'src/app/service/user.service';
import { RealtimeDatabaseService } from 'src/app/storage/realtime-database.service';

// @UntilDestroy()

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  userData = [{}];
  user: any;
  user$ = this.userService.currentUserProfile$;
  profileForm = this.fb.group({
    uid: ['', Validators.required],
    displayName: ['', Validators.required],
    firstName: ['', Validators.required],
    dateOB:['', Validators.required],
    gender:['', Validators.required],
    lastName: ['', Validators.required],
    status:['', Validators.required],
    phone: ['', Validators.required],
    address: ['', Validators.required],
  });
  constructor(
    private usersService: UserService,
    private formBuilder: FormBuilder,
    private navCont: NavController,
    private router: Router,
    private realtimeD: RealtimeDatabaseService,
    private userService: UserService,
    private fb: NonNullableFormBuilder,
    private firestore: Firestore,
    private imageUploadService:ImageUploadService
  ) {
    //To take info from navigation
    //  this.detail = this.router.getCurrentNavigation()?.extras?.state;
    //  this.inputVal=this.detail.nam
    //  this.detail=this.detail.donne
    
  }
 
  uploadFile(event: any, { uid }: ProfileUser) {
    this.imageUploadService
      .uploadImage(event.target.files[0], `images/profile/${uid}`)
      .pipe(
        // this.toast.observe({
        //   loading: 'Uploading profile image...',
        //   success: 'Image uploaded successfully',
        //   error: 'There was an error in uploading the image',
        // }),
        switchMap((photoURL:any) =>
          this.usersService.updateUser({
            uid,
            photoURL,
          })
        )
      )
      .subscribe();
  }


  // uploadFile(event: any, { uid }: ProfileUser) {
  //   this.imageUploadService
  //     .uploadImage(event.target.files[0], `images/profile/${uid}`)
  //     .pipe(
  //       this.toast.observe({
  //         loading: 'Uploading profile image...',
  //         success: 'Image uploaded successfully',
  //         error: 'There was an error in uploading the image',
  //       }),
  //       switchMap((photoURL) =>
  //         this.usersService.updateUser({
  //           uid,
  //           photoURL,
  //         })
  //       )
  //     )
  //     .subscribe();
  // }

  // async setProfil() {
  //   const datas = await this.setVal();
  //   const claimer =await this.realtimeD.saveUser('lecture', datas);
  //   if (claimer) {
  //   this.navCont.navigateBack('/liste-associations',);
  //   }

  // }

  saveProfile() {
    const { uid, ...data } = this.profileForm.value;

    if (!uid) {
      return;
    }

    this.usersService
      .updateUser({ uid, ...data })
      .pipe
      // this.toast.observe({
      //   loading: 'Saving profile data...',
      //   success: 'Profile updated successfully',
      //   error: 'There was an error in updating the profile',
      // })
      ()
      .subscribe();
  }

  ngOnInit() {
    this.usersService.currentUserProfile$
      .pipe(
        // untilDestroyed(this),
        tap(console.log)
      )
      .subscribe((user) => {
        this.profileForm.patchValue({ ...user });
      });
  }
}
