import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  LoadingController,
  NavController,
  ToastController,
} from '@ionic/angular';
import { switchMap } from 'rxjs';
import { AuthService } from 'src/app/service/authService';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {
  monFormulaire: FormGroup;
  user: any;
  loading: any;
  constructor(
    private loadingController: LoadingController,
    private userService: UserService,
    private authservice: AuthService,
    public toastController: ToastController,
    private formBuilder: FormBuilder,
    public navCont: NavController
  ) {
    this.monFormulaire = this.formBuilder.group(
      {
        nom: ['', Validators.required],
        // rPrenom: ['', Validators.required],
        // rDate: ['', Validators.required],
        // rSexe: ['', Validators.required],
        // rAdresse: ['', Validators.required],
        // rTEL: ['', Validators.required],
        // mDP: ['', Validators.required],
        // mDPC: ['', Validators.required],
        // rStatut: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        mDP: ['', [Validators.required, Validators.minLength(6)]],
        mDPC: ['', [Validators.required, Validators.minLength(6)]],
      },
      {
        validator: this.matchPassword, // Ajoutez cette ligne
      }
    );
  }

  matchPassword(formulaire: FormGroup) {
    return formulaire.value.mDP === formulaire.value.mDPC
      ? null
      : { misMatch: true };
  }

  async submitForm() {
    this.user = await this.authservice.signUp(
      this.monFormulaire.value.email,
      this.monFormulaire.value.mDP
    );

    if (this.user) {
      // Soumettre le formulaire
      this.navCont.navigateForward('/liste-associations');
    }
  }
  goBack() {
    this.navCont.back();
  }

  ngOnInit() {}

  async submit() {
    const { nom, email, mDP } = this.monFormulaire.value;

    if (this.monFormulaire.valid) {
      this.loading = await this.loder();
      await this.loading.present();
      try {
        this.authservice
          .signUp(email, mDP)
          .pipe(
            switchMap(({ user: { uid } }) =>
              this.userService.addUser({ uid, email, displayName: nom })
            )
          )
          .subscribe(async () => {
            await this.loading.dismiss();
            this.presentToast('Enreisetrement effectuer avec succè', 'success');
            this.navCont.navigateForward('/liste-associations');
          });
      } catch (e) {
        await this.loading.dismiss();
        await this.presentToast('Veuillez réessayer', 'danger');
        // console.error('Error during registration:', e);
        //     return null;
      }
      return;
    }
  }

  async presentToast(messages: string, colors: string) {
    const toast = await this.toastController.create({
      message: messages,
      duration: 2000,
      color: colors,
      // position: position,
    });

    await toast.present();
  }

  async loder() {
    return await this.loadingController.create({
      cssClass: 'my-custom-class', // définir le style dans le fichier CSS
      message: 'Veuillez patienter...', // afficher un message
      spinner: 'circles', // utiliser le type circles
      translucent: true, // rendre le fond transparent
    });
  }
}
