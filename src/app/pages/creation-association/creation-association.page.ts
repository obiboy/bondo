import { Component, Injectable, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { NonNullableFormBuilder, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { of, switchMap } from 'rxjs';
import { ProfileUser } from 'src/app/model/user';
import { AssociationService } from 'src/app/service/association.service';
import { ImageUploadService } from 'src/app/service/image-upload.service';
import { UserService } from 'src/app/service/user.service';

@Injectable({
  providedIn: 'root',
})
@Component({
  selector: 'app-creation-association',
  templateUrl: './creation-association.page.html',
  styleUrls: ['./creation-association.page.scss'],
})
export class CreationAssociationPage implements OnInit {
  user$ = this.usersService.currentUserProfile$;
  userId: any;
  assocForm = this.fb.group({
    // aId:   ['', Validators.required],
    name: ['', Validators.required],
    description: ['', Validators.required],
    headquater: ['', Validators.required],
    tel: ['', Validators.required],
    email: ['', Validators.required],
    president: ['', Validators.required],
    // phoneN:  ['', Validators.required],
    // address:  ['', Validators.required],
  });

  constructor(
    private fb: NonNullableFormBuilder,
    private imageUploadService: ImageUploadService,
    public navCont: NavController,
    private usersService: UserService,
    private assocService: AssociationService,
    private db: AngularFireDatabase
  ) {}
  //  le quelle est mieux

  //   ngOnInit() {
  //     this.user$.subscribe(
  //       (user)=>
  //         this.userId=user?.uid

  //     )
  //   }
  //   ou
  ngOnInit() {
    this.user$
      .pipe(switchMap((user) => of((this.userId = user?.uid))))
      .subscribe();
  }
  validLog() {
    this.navCont.navigateForward('/tab-p');
    console.log('this is the name : ');
  }
  uploadFile(event: any, { uid }: ProfileUser) {
    this.imageUploadService
      .uploadImage(event.target.files[0], `images/associati on/${uid + 'img'}`)
      .pipe
      // this.toast.observe({
      //   loading: 'Uploading profile image...',
      //   success: 'Image uploaded successfully',
      //   error: 'There was an error in uploading the image',
      // }),
      // switchMap((photoURL: any) =>
      //   this.assocService.updateAssociation({
      //     aId,
      //     photoURL,
      //   })
      // )
      ()
      .subscribe();
  }
  creatAssoc() {
    const aIds = this.db.createPushId();
    // const aIds = '-Nob7H1pX35VnI0VhlBr';
    const { name, ...data } = this.assocForm.value;
    if (name) {
      // this.assocService.creatAssociation({aId:aIds,name,members:[{uid:this.userId,poste:'Préident'}],...data}).pipe(
      //   switchMap((rep)=>
      //   this.usersService.updateUser({uid:this.userId,associations:[aIds]})
      //   )
      // ).subscribe()
      this.assocService.creatAssociation({
        aId: aIds,
        name,
        // members: [{ uid: 'this.userId', poste: 'Membre' }],
        ...data,
      });
    }
    // else{ }
    this.navCont.navigateBack;
  }
  addMember(){
    const aIds = '-Nob7H1pX35VnI0VhlBr';

    this.assocService.addMember({aId:aIds,name:'soooooooo'})
  }
  updateMember(){
    const aIds = '-Nob7H1pX35VnI0VhlBr';

    this.assocService.updateMember({aId:aIds,name:'soooooooo'})
  }
}
