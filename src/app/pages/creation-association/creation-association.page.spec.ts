import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreationAssociationPage } from './creation-association.page';

describe('CreationAssociationPage', () => {
  let component: CreationAssociationPage;
  let fixture: ComponentFixture<CreationAssociationPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CreationAssociationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
