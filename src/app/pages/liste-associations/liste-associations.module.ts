import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgControl, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, NavParams } from '@ionic/angular';

import { ListeAssociationsPageRoutingModule } from './liste-associations-routing.module';

import { ListeAssociationsPage } from './liste-associations.page';
// import { UserService } from 'src/app/service/user.service'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListeAssociationsPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ListeAssociationsPage],
  providers: [NavParams,
    // UserService
  ] ,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ListeAssociationsPageModule {}
