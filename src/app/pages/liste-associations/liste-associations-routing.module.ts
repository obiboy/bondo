import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListeAssociationsPage } from './liste-associations.page';

const routes: Routes = [
  {
    path: '',
    component: ListeAssociationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListeAssociationsPageRoutingModule {}
