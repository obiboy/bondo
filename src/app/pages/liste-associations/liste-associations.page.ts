import { Component, OnInit } from '@angular/core';
import { Firestore, doc } from '@angular/fire/firestore';
import { FormControl, NgControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController, NavParams } from '@ionic/angular';
import {
  Observable,
  combineLatest,
  concatMap,
  distinctUntilChanged,
  map,
  mergeAll,
  of,
  startWith,
  switchMap,
  take,
  toArray,
} from 'rxjs';
import { AssociationModel } from 'src/app/model/association';
import { CurrentAssoc } from 'src/app/model/currentData/currentAssoc';
import { AssociationService } from 'src/app/service/association.service';
import { AuthService } from 'src/app/service/authService';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-liste-associations',
  templateUrl: './liste-associations.page.html',
  styleUrls: ['./liste-associations.page.scss'],
  providers: [FormControl],
})
export class ListeAssociationsPage implements OnInit {
  // testesObsrvable = of({
  //   joe: 'name joe',
  //   listes: ['liste1', 'liste2', 'liste3', 'liste4'],
  // });
  // bins=this.testesObsrvable.pipe(

  //   switchMap((rep:any)=>{
  //     if(rep&&rep.listes){
  //       return rep.listes
  //     }
  //   })
  // )
  listesT: Array<Observable<AssociationModel>> = [];
  user: any;
  searchTerm = '';
  searchControl = new FormControl('');
  // searchControl:string=''
  inputVal: any;
  testss: any;
  filtereditems$: any;
  associations: any;

  user$ = this.userService.currentUserProfile$;
  listesTs = this.user$.pipe(
    map((userData) => {
      if (userData && userData.associations) {
        console.log('In contactMap' + userData.associations);
        return userData.associations.map((rep: string) =>
          this.allAssociations.associationById(rep)
        );
      } else {
        // Si l'utilisateur n'a pas d'associations, retourner un observable vide ou un tableau vide
        return [];
      }
    })
  );
  joe() {
    this.user$
      .pipe(
        concatMap((userData) => {
          if (userData && userData.associations) {
            console.log('In contactMap' + userData.associations);

            // const listeAssoci= this.allAssociations(userData.associations)

            // Créer un tableau d'observables pour chaque association
            // const associationObservables = userData.associations.map(id =>
            //   doc(this.firestore,'association',id),

            // );
            // console.log('aaaaa:  '+associationObservables)
            // Combiner les observables en un seul observable pour obtenir toutes les associations
            // return combineLatest([associationObservables]);
            return userData.associations;
          } else {
            // Si l'utilisateur n'a pas d'associations, retourner un observable vide ou un tableau vide
            return [];
          }
        })
      )
      .subscribe((rep) => {
        this.associations = rep;
        console.log('rep:  ' + rep);
      });
  }

  //////////////////////////////
  async demadeAlert(ac: AssociationModel) {
    console.log(ac.name);
    const alert = await this.alertController.create({
      header: "Faire une demande d'adhesion",
      // backdropDismiss: false,

      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Opération annulée');
          },
        },
        {
          text: 'Demande',
          handler: (a) => {
            this.allAssociations.demande(ac), this.searchControl.reset();
            // try{const joe= this.authService.PasswordRecover(data.emailR)}catch(e){console}
            // Utilisez les valeurs des champs de saisie ici
          },
        },
      ],
    });

    await alert.present();
  }
  ///////////////seacheAssoc///////////////////
  assocs$ = combineLatest([
    this.allAssociations.allAsso$,
    this.searchControl.valueChanges.pipe(startWith('')),
  ]).pipe(
    map(([users, searchString]) => {
      if (!searchString) {
        return []; // Return an empty array if searchString is empty
      }
      return users.filter((u) =>
        u.name?.toLowerCase().includes(searchString.toLowerCase())
      );
    })
  );
  ////////////////seacheUser//////////
  alluser$ = this.userService.allUsers$;
  users$ = combineLatest([
    this.userService.allUsers$,
    this.user$,
    this.searchControl.valueChanges.pipe(startWith('')),
  ]).pipe(
    map(([users, user, searchString]) => {
      if (!searchString) {
        return []; // Return an empty array if searchString is empty
      }
      return users.filter(
        (u) =>
          u.displayName?.toLowerCase().includes(searchString.toLowerCase()) &&
          u.uid !== user?.uid
      );
    })
  );

  constructor(
    public navCont: NavController,
    private router: Router,
    private navParams: NavParams,
    private authService: AuthService,
    private route: ActivatedRoute,
    private currentAssoc: CurrentAssoc,
    private userService: UserService,
    private firestore: Firestore,
    private allAssociations: AssociationService,
    private alertController: AlertController
  ) {
    this.user$
      .pipe(
        concatMap((userData) => {
          if (userData && userData.associations) {
            console.log('In contactMap' + userData.associations);

            // const listeAssoci= this.allAssociations(userData.associations)

            // Créer un tableau d'observables pour chaque association
            // const associationObservables = userData.associations.map(id =>
            //   doc(this.firestore,'association',id),

            // );
            // console.log('aaaaa:  '+associationObservables)
            // Combiner les observables en un seul observable pour obtenir toutes les associations
            // return combineLatest([associationObservables]);
            return userData.associations;
          } else {
            // Si l'utilisateur n'a pas d'associations, retourner un observable vide ou un tableau vide
            return [];
          }
        })
      )
      .subscribe((rep) => {
        console.log('rep:  ' + rep);
        this.listesT.push(this.allAssociations.associationById(rep));
      });
  }

  reciveAssoc(a: Observable<AssociationModel>) {
    this.currentAssoc.parametre = a;
    a.subscribe((rep) => {
      console.log(rep.name);
    });
    
    // Pour sauvegarder des données

    const subs = a.subscribe((dataA) => {
      sessionStorage.setItem('myData', JSON.stringify(dataA));
      this.navCont.navigateForward('/tab-p');
    console.log('Passage au tab-P');
    });
    // subs.unsubscribe();
  }

  ngOnInit() {}

  filterItems() {
    console.log(this.searchTerm);
    if (this.searchTerm.trim() !== '') {
      this.filtereditems$ = this.alluser$.pipe(
        map((users: any[]) => {
          return users.filter((user: any) => {
            return (
              user.displayName
                .toLowerCase()
                .includes(this.searchTerm.toLowerCase()) &&
              user.uid !== this.user?.uid > -1
            );
          });
        })
      );
    } else {
      this.filtereditems$ = []; // Réinitialiser la liste des éléments filtrés lorsque le terme de recherche est vide
    }
  }

  //     text: 'Delete',
  //     role: 'destructive',
  //     data: {
  //       action: 'delete',
  //     },
  //   },
  //   {
  //     text: 'Share',
  //     data: {
  //       action: 'share',
  //     },
  //   },
  //   {
  //     text: 'Cancel',
  //     role: 'cancel',
  //     data: {
  //       action: 'cancel',
  //     },
  //   },
  // ];
  focusSearch() {
    const searchBar = document.getElementById(
      'searchBar'
    ) as HTMLIonSearchbarElement;
    if (searchBar) {
      searchBar.setFocus();
    }
  }
  //////////MenuOptions///////
  goTo(choix: number) {
    switch (choix) {
      case 1:
        this.navCont.navigateForward('/edit-profile');
        console.log('Premier choix');
        break;
      case 2:
        this.navCont.navigateForward('/porte-monnaie');
        console.log('Deuxième choix');
        break;
      case 3:
        this.navCont.navigateForward('/liste-associations');
        console.log('Troisième choix');
        break;
      case 4:
        this.navCont.navigateForward('/mes-notification');
        console.log('Troisième choix');
        break;
      case 5:
        this.navCont.navigateForward('/condition');
        console.log('Troisième choix');
        break;
      case 6:
        this.navCont.navigateForward('/a-propos');
        console.log('Troisième choix');
        break;

      default:
        break;
    }
  }
  goToCreate() {
    this.navCont.navigateForward('/creation-association');
    console.log('this is the name : ');
  }
  logOut() {
    this.authService.SignOut();
    this.navCont.navigateForward('/testelog');
  }
  /////////////////////////
}
