import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProjetPPageRoutingModule } from './projet-p-routing.module';

import { ProjetPPage } from './projet-p.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProjetPPageRoutingModule
  ],
  declarations: [ProjetPPage]
})
export class ProjetPPageModule {}
