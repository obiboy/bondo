import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProjetPPage } from './projet-p.page';

describe('ProjetPPage', () => {
  let component: ProjetPPage;
  let fixture: ComponentFixture<ProjetPPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ProjetPPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
