// chat-group.service.ts
import { Injectable } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable, switchMap } from 'rxjs';
import { AuthService } from './authService';
import { CurrentAssoc } from '../model/currentData/currentAssoc';

@Injectable({
  providedIn: 'root',
})
export class ChatGroupService {
  uUid:any
  constructor(
    private db: AngularFireDatabase,
    private authService: AuthService,
    private currentAssoc:CurrentAssoc
  ) {}
  userUID() {
     this.authService.currentUser$.subscribe((users)=>{
      if(users){
        this.uUid= users?.uid;  
      }
      })
  }
  groupKey = 'voideOfAny'
  createChatGroup(groupName: string) {
    // Generate a unique key for the chat group
    // const groupKey = this.db.createPushId();
    const groupKey = 'voideOfAny';

    // Create a reference to the chat group
    // const groupRef = this.db.object(`chatGroups/${groupKey}`);
    const groupRef = this.db.object(`chatGroups/${groupKey}`);

    // Set the chat group data
    groupRef.set({
      groupName: groupName,
      // participants: participants,
      messages: [], // Initialize messages array
    });

    console.log(
      `Chat group '${groupName}' created successfully with key '${groupKey}'.`
    );
  }

  // sendMessage(groupKey: string, senderId: string, message: string) {
  async sendMessage(message: string,senderId :string) {
    const messageData = {
      senderId: senderId,
      message: message,
      timestamp: new Date().getTime(),
    };

    // Push the message to the chat group's messages array
    this.db.list(`chatGroups/${this.groupKey}/messages`).push(messageData);
  }

  get messages(): Observable<any[]> {
    // Return an observable of the chat group's messages
    return this.db.list(`chatGroups/${this.groupKey}/messages`).valueChanges();
  }
}
