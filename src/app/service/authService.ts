import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {
  Auth,
  authState,
  createUserWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithEmailAndPassword,
  signOut,
  User,
} from '@angular/fire/auth';
import firebase from 'firebase/compat/app';
import { getAuth, RecaptchaVerifier, UserCredential } from 'firebase/auth';
import { LoadingController, ToastController } from '@ionic/angular';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  currentUser$ = authState(this.auth);
  
  confirmationResult: firebase.auth.ConfirmationResult | null = null;
  confirmC: any;
  loading: any;
  constructor(
    private auth: Auth,
    public afAuth: AngularFireAuth,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) {}
  /////////createUserWithEmailAndPassword//////
  signUp(email: string, password: string): Observable<UserCredential> {
    return from(createUserWithEmailAndPassword(this.auth, email, password));
  }
  ////////signInWithEmailAndPassword/////////////
  login(email: string, password: string): Observable<any> {
    return from(signInWithEmailAndPassword(this.auth, email, password));
  }
  ////////signInWithEmailAndPassword/////////////
  googleAuth(): Observable<any> {
    return from(  this.afAuth.signInWithPopup(new GoogleAuthProvider())
    );
  }

  // async register(email: string, password: string): Promise<Observable<UserCredential>> {
  //   try {
  //     this.loading = await this.loder();
  //     await this.loading.present()
  //     return
  //      from (createUserWithEmailAndPassword(
  //       this.auth,
  //       email,
  //       password
  //     ));
  //     this.loading.dismiss();
  //     this.presentToast('Vous avez été enregistrer avec succès', 'success');
  //   } catch (e) {
  //     this.loading.dismiss();
  //     this.presentToast('Veuillez réessayer', 'danger');
  //     console.error('Error during registration:', e);
  //     return null;
  //   }
  // }
  ///////////////***End createUserWithEmailAndPassword///////////////////////
  //////////////signInWithEmailAndPassword/////////////
  async logIn(email: string, password: string): Promise<User | null> {
    try {
      this.loading = await this.loder();
      await this.loading.present();
      const userCredential = await signInWithEmailAndPassword(
        this.auth,
        email,
        password
      );
      this.loading.dismiss();
      return userCredential.user;
    } catch (e) {
      this.loading.dismiss();
      this.presentToast('Veuillez réessayer', 'danger');
      console.error('Error during login:', e);
      return null;
    }
  }
  ////////////***End signInWithEmailAndPassword//////////////////
  //////////loginWithGoogle///////////

  async loginWithGoogle() {
    try {
      this.loading = await this.loder();
      await this.loading.present();
      const Cuser = await this.afAuth.signInWithPopup(new GoogleAuthProvider());
      await this.loading.dismiss();
      return Cuser.user;
    } catch (e:any) {
      await this.loading.dismiss();
          switch (e.code) {
            case 'auth/network-request-failed':
              this.presentToast(
                'Veuillez vérifier votre connexion internet',
                'danger'
              );
              break;
            case 'auth/popup-closed-by-user':
              this.presentToast(
                'Le popup a été fermé',
                'danger'
              );
              break;
            case 'auth/popup-blocked':
              this.presentToast(
                'Le popup a été bloqué',
                'danger'
              );
              break;

              
            default:
                  this.presentToast('Veuillez réessayer', 'danger');

          }
      console.log(e);
      return null;
    }
  }
  ////////**End loginWithGoogle////////////////////////////

  //////////////phone log//////////////////////
  async sendVerificationCode(number: string): Promise<any> {
    const auth = getAuth();
    const appVerifier = new RecaptchaVerifier(
      getAuth(),
      'recaptcha-container',
      {
        size: 'invisible',
        callback: (response: any) => {},
      }
    );
    try {
      this.loading = await this.loder();
      await this.loading.present();
      this.confirmC = await this.afAuth.signInWithPhoneNumber(
        number,
        appVerifier
      );
      await this.loading.dismiss();
      // Handle confirmation, get verification code, etc.
      this.presentToast('Code de confirmation envoyer', 'primary');
      console.log('Code Sent:', this.confirmationResult);
    } catch (error) {
      await this.loading.dismiss();
      this.presentToast('Veuillez réessayer', 'danger');
      console.error('Error sending code:', error);
    }
  }

  async verifyCode(code: string): Promise<any> {
    try {
      {
        this.loading = await this.loder();
        // await this.loading.present();
        const credential = await this.confirmC.confirm(code);
        this.loading.dismiss();
        return credential;
      }
    } catch (error) {
      await this.loading.dismiss();
      this.presentToast('Veuillez réessayer', 'danger');
      console.error('Error verifying code:', error);
      // if (error instanceof SyntaxError) {
      //   // gérer l'erreur de syntaxe
      //   this.presentToast('Erreur de syntaxe');
      // } else if (error instanceof TypeError) {
      //   // gérer l'erreur de type
      //   this.presentToast('Erreur de type');
      // } else if (error instanceof ReferenceError) {
      //   // gérer l'erreur de référence
      //   this.presentToast('Erreur de référence');
      // } else if (error instanceof LogicError) {
      //   // gérer l'erreur de logique
      //   this.presentToast('Erreur de logique');
      // } else if (error instanceof NetworkError) {
      //   // gérer l'erreur de réseau
      //   this.presentToast('Erreur de réseau');
      // } else if (error instanceof LoadingError) {
      //   // gérer l'erreur de chargement
      //   this.presentToast('Erreur de chargement');
      // } else if (error instanceof ToastError) {
      //   // gérer l'erreur de toast
      //   this.presentToast('Erreur de toast');
      // } else {
      //   // gérer l'erreur inconnue
      //   this.presentToast('Erreur inconnue');
      // }
    }
  }
  //////////////**End phone log/////////////////////

  async PasswordRecover(passwordResetEmail: any) {
    this.loading = await this.loder();
    await this.loading.present();
    return this.afAuth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        this.loading.dismiss();
        this.presentToast(
          'E-mail de recuperation de mot de passe enoyé',
          'success'
        );
      })
      .catch((error) => {
        this.loading.dismiss();
        this.presentToast('Veuillez réessayer', 'danger');
        //   switch (error.code) {
        //     case 'auth/invalid-email':
        //       this.presentToast('Veuillez entrer une adresse e-mail valide');
        //       break;
        //     case 'auth/user-not-found':
        //       this.presentToast(
        //         'Aucun utilisateur correspondant à cette adresse e-mail'
        //       );
        //       break;
        //     case 'auth/too-many-requests':
        //       this.presentToast(
        //         'Trop de demandes ont été faites au serveur. Veuillez réessayer plus tard'
        //       );
        //       break;
        //     case 'auth/network-request-failed':
        //       this.presentToast(
        //         "Une erreur réseau s'est produite. Veuillez vérifier votre connexion internet et la disponibilité du serveur"
        //       );
        //       break;
        //     default:
        //       this.presentToast("Erreur d'envoi de mail: " + error.message);
        //   }
      });
  }
  async presentToast(messages: string, colors: string) {
    const toast = await this.toastController.create({
      message: messages,
      duration: 2000,
      color: colors,
      // position: position,
    });

    await toast.present();
  }
  async loder() {
    return await this.loadingController.create({
      cssClass: 'my-custom-class', // définir le style dans le fichier CSS
      message: 'Veuillez patienter...', // afficher un message
      spinner: 'circles', // utiliser le type circles
      translucent: true, // rendre le fond transparent
    });
  }
  async SignOut() {
    await this.afAuth.signOut();
    alert('Logged out!');
  }
}
