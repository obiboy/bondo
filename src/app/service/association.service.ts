import { Injectable } from '@angular/core';
import { AssociationModel } from '../model/association';
import { Observable, firstValueFrom, from } from 'rxjs';
import {
  Firestore,
  addDoc,
  arrayUnion,
  collection,
  collectionData,
  doc,
  docData,
  getDoc,
  query,
  setDoc,
} from '@angular/fire/firestore';
import { updateDoc } from 'firebase/firestore';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class AssociationService {
  uidd:any
  cName:any
  constructor(private firestore: Firestore,private userSer:UserService) {}

  get allAsso$():Observable<AssociationModel []>{
    const ref = collection(this.firestore,'association',)
    const queryAll=query(ref)
    return collectionData(queryAll) as Observable<AssociationModel[]>
  }
  async demande(assoc:AssociationModel){
  //  await this.userSer.currentUserProfile$.subscribe(vlal=>{
  //     this.uidd= vlal?.uid;
  //     this.cName= vlal?.firstName;
  //     })
  const vlal = await firstValueFrom(this.userSer.currentUserProfile$);
  this.uidd = vlal?.uid;
  this.cName = vlal?.firstName;

  const ref = doc(this.firestore, 'association', assoc.aId);
  const demandeCollection = doc(ref, 'Demande',this.uidd);
  setDoc(demandeCollection, { uid:this.uidd,name:this?.cName, poste: 'Membre' });    
  }
  
  async creatAssociation(asso: AssociationModel) {
     this.userSer.currentUserProfile$.subscribe(vlal=>{
      this.uidd= vlal?.uid;
      this.cName= vlal?.firstName;
      })

    const ref = doc(this.firestore, 'association', asso.aId);
    await setDoc(ref, asso);
    // collection(ref, 'Members');
    const membersCollection = collection(ref, 'Members');
    const demandeCollection = collection(ref, 'Demande');
    addDoc(demandeCollection, {});
    addDoc(membersCollection, { uid:this.uidd,nme:this.cName, poste: 'Secretaire' });
    // this.addMember(asso);
    return Promise.resolve();
  }

  associationById(idA:string): Observable<AssociationModel>{
    const ref = doc(this.firestore,'association',idA)
    // const queryAll=query(ref)
    return docData(ref) as Observable<AssociationModel>
  }

  updateAssociation(asso: AssociationModel): Observable<void> {
    const ble = this.firestore;
    const ref = doc(this.firestore, 'association', asso.aId);
    return from(updateDoc(ref, { ...asso }));
  }
  // async addMember(asso: AssociationModel) {
  //   const ble= this.firestore
  //   const ref = doc(this.firestore, 'association', asso.aId);
  //   // const associationDoc= await getDoc(ref)
  //   const newMember= {uid:'lenouveauJOE',poste:'Secretaire'};
  //   // if(associationDoc.exists()){
  //   //   const membresAcctuel=associationDoc.data()['members'];
  //   //   const updatMemb= arrayUnion(membresAcctuel,newMember);
  //   //   asso.members?.push(newMember)

  //   // }
  //   return from(updateDoc(ref, {members:arrayUnion(newMember)}));
  // }
  async addMember(asso: AssociationModel) {
    const ref = doc(this.firestore, 'association', asso.aId);
    console.log(asso.aId)
    const membersCollection = collection(ref, 'Members');
    addDoc(membersCollection, { uid: 'joooooeeee', poste: 'Président' });
    return;
  }
  async updateMember(asso: AssociationModel) {
    try {
      const ref = doc(this.firestore, 'association', asso.aId);
      const membersCollection = collection(ref, 'Members');
      const refDoc = doc(membersCollection);
      updateDoc(refDoc, {
        uid: 'joooooeeee',
        poste: 'Tresorier',
        nothing: 'nothing',
      });
      return;
    } catch (error) {
      console.error(error);
    }
  }
  // Déclarer la fonction comme asynchrone avec le mot-clé async
  async pdateMember(asso: AssociationModel) {
    // Utiliser un bloc try pour capturer les erreurs potentielles
    try {
      // Utiliser le mot-clé await pour attendre la résolution des promesses
      const ref = await doc(this.firestore, 'association', asso.aId);
      const membersCollection = await collection(ref, 'Members');
      const refDoc = await doc(membersCollection, 'joooooeeee');
      await updateDoc(refDoc, {
        uid: 'joooooeeee',
        poste: 'Tresorier',
        nothing: 'nothing',
      });
      // Retourner une valeur ou une promesse si nécessaire
      return;
    } catch (error) {
      // Gérer les erreurs avec le bloc catch
      console.error(error);
      // Lancer une nouvelle erreur ou retourner une valeur ou une promesse si nécessaire
      throw error;
    }
  }
}
