import { Injectable } from '@angular/core';
import { Firestore, collectionData, doc, docData, getDoc, query, setDoc, updateDoc } from '@angular/fire/firestore';
import { Observable, from, of, switchMap } from 'rxjs';
import { ProfileUser } from '../model/user';
import { AuthService } from './authService';
import { collection } from 'firebase/firestore';
// import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user:any
  constructor(private authService:AuthService, private firestore:Firestore) { }
  
  get currentUserProfile$(): Observable<ProfileUser | null> {
    return this.authService.currentUser$.pipe(
      switchMap((users) => {
        if (!users?.uid) {
          return of(null);
        }
        const ref = doc(this.firestore, 'users', users?.uid);
        return docData(ref) as Observable<ProfileUser>;
      })
    );
  }
  get allUsers$():Observable<ProfileUser []>{
    const ref = collection(this.firestore,'users',)
    const queryAll=query(ref)
    return collectionData(queryAll) as Observable<ProfileUser[]>
  }
  

  async checkProfile(userId: string,name:string) {
  const userRef =doc(this.firestore, 'users', userId)

  // Check if the document exists
  const docSnapshot = await getDoc(userRef);
  if (docSnapshot.exists()) {
    console.log(`Document for user ${userId} already exists.`);
    // Document exists, fetch and return data
    // return docData(userRef);
  } else {
    console.error(`No such document for user ${userId}`);
    this.addUser({uid:userId,displayName:name})
    // Document doesn't exist, return null
    // return null;
  }
}
  addUser(user: ProfileUser): Observable<void> {
    const ref = doc(this.firestore, 'users', user.uid);
    return from(setDoc(ref, user));
  } 
  updateUser(user: ProfileUser): Observable<void> {
    const ref = doc(this.firestore, 'users', user.uid);
    return from(updateDoc(ref, {...user}));
  }

}
