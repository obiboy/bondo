import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  NgControl,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import {
  AlertController,
  LoadingController,
  NavController,
  NavParams,
} from '@ionic/angular';
import { AuthService } from '../service/authService';
import { environment } from 'src/environments/environment';
import { User } from '@angular/fire/auth/firebase';
import { UserService } from '../service/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  aUser: any;
  CountryJson = environment.CountryJoson;
  Code: any;
  PhoneNo: any;
  CountryCode: any = '+241';
  
  formNum: FormGroup;
  formCode: FormGroup;
  verificationC: any;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    // private loadingController: LoadingController,
    // private alertController: AlertController,
    // // @Inject('authService')
    // private authService: AuthService,
    // private router: Router
    private alertController: AlertController,
    private authService: AuthService,
    private userService :UserService
  ) {
    this.formNum = this.fb.group({
      phoneNum: ['', [Validators.required, Validators.minLength(8)]],
    });
    this.formCode = this.fb.group({
      fCode: ['', [Validators.required, Validators.minLength(6)]],
    });
    console.log(this.formNum.value.toString());
  }

  ngOnInit() {
    // this.inputemail=Validators.email
  }

  countryCodeChange($event: any) {
    this.CountryCode = $event.detail.value;
  }
  //   // Button event after the nmber is entered and button is clicked
  async signinWithPhoneNumber() {
    this.verificationC = await this.authService.sendVerificationCode(
      this.CountryCode + this.formNum.value.phoneNum
    );
    console.log(this.CountryCode);
  }
 
  async codeVerif() {
    try {
      const credentials = await this.authService.verifyCode(
        this.formCode.value.fCode
      );
      if (credentials.user) {
        this.aUser = credentials.user;
      await this.userService.checkProfile(credentials.user.uid,'User')

        this.router.navigateByUrl('/liste-associations');
        console.log(credentials.user);
      }
    } catch (error) {
      console.log('le problem', error);
    }
  }
 }
