import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabCommPage } from './tab-comm.page';

describe('TabCommPage', () => {
  let component: TabCommPage;
  let fixture: ComponentFixture<TabCommPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TabCommPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
