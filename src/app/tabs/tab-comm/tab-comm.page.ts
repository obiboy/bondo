import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { AuthService } from 'src/app/service/authService';
import { ChatGroupService } from 'src/app/service/chat.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-tab-comm',
  templateUrl: './tab-comm.page.html',
  styleUrls: ['./tab-comm.page.scss'],
  providers:[DatePipe]
})
export class TabCommPage implements OnInit {
  groupKey: string = 'voideOfAny'; // Replace with the actual group key
  // messages$: Observable<any[]>;
  messages$: any;
  user$ = this.userService.currentUserProfile$;

  newMessage: string = '';
  senderId: any;
  

  constructor(private datePipe:DatePipe,private chatGroupService: ChatGroupService,private userService:UserService,private authService:AuthService) {}

  ngOnInit() {
    // Retrieve messages when the component initializes
    this.messages$ = this.chatGroupService.messages;
    this.authService.currentUser$.subscribe((users)=>{
      if(users){
        this.senderId= users?.uid;  
      }
      })
  }
  formatTimestamp(timestamp: number): string {
        // Format the timestamp using DatePipe
            return this.datePipe.transform(timestamp, 'medium') ||''; // Adjust the format as needed
  }
  

  sendMessage() {
    // const senderId =this.user$?.uid; // Replace with the actual user ID
    this.chatGroupService.sendMessage(this.newMessage,this.senderId);
    this.newMessage = ''; // Clear the input field after sending a message
  }
}
