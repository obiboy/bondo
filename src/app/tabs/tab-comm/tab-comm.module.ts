import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabCommPageRoutingModule } from './tab-comm-routing.module';

import { TabCommPage } from './tab-comm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabCommPageRoutingModule
  ],
  declarations: [TabCommPage]
})
export class TabCommPageModule {}
