import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab-activite',
  templateUrl: './tab-activite.page.html',
  styleUrls: ['./tab-activite.page.scss'],
})
export class TabActivitePage implements OnInit {

  constructor(public navCont:NavController) { }

  ngOnInit() {
  }
  goTo(choix: number) {
    switch (choix) {
      case 1:
        this.navCont.navigateForward('/liste-membre');
        console.log('Premier choix');
        break;
      case 2:
        this.navCont.navigateForward('/liste-projet');
        console.log('Deuxième choix');
        break;
      case 3:
        this.navCont.navigateForward('/liste-evenement');
        console.log('Troisième choix');
        break;
      case 4:
        this.navCont.navigateForward('/liste-dons');
        console.log('Troisième choix');
        break;
      case 5:
        this.navCont.navigateForward('/caisse');
        console.log('Troisième choix');
        break;
      case 6:
        this.navCont.navigateForward('/liste-documents');
        console.log('Troisième choix');
        break;

      default:
        break;
    }
  }

}
