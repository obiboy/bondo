import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabActivitePage } from './tab-activite.page';

describe('TabActivitePage', () => {
  let component: TabActivitePage;
  let fixture: ComponentFixture<TabActivitePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TabActivitePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
