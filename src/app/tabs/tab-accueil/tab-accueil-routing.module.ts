import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabAccueilPage } from './tab-accueil.page';

const routes: Routes = [
  {
    path: '',
    component: TabAccueilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabAccueilPageRoutingModule {}
