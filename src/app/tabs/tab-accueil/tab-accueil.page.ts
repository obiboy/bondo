import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { CurrentAssoc } from 'src/app/model/currentData/currentAssoc';
import { SwiperOptions } from 'swiper/types/swiper-options';

@Component({
  selector: 'app-tab-accueil',
  templateUrl: './tab-accueil.page.html',
  styleUrls: ['./tab-accueil.page.scss'],
})
export class TabAccueilPage implements OnInit {
  catOption: SwiperOptions = {
    // freeMode:true,
    // slidesPerView:2,
    // slidesOffsetBefore:11
  };

  currentCat = this.currentAssoc.parametre;
  currAssoc:any
  tabl = [1, 2, 3, 4, 5, 6];

  constructor(
    public navCont4: NavController,
    private alertController: AlertController,
    private currentAssoc: CurrentAssoc
  ) {
    // this.currentAssoc.parametre?.subscribe(rep=>console.log('jojjojojo'+rep.name))
    
  }

  ngOnInit() {
    const assoc = sessionStorage.getItem('myData');

    if (assoc !== null) {
      this.currAssoc = JSON.parse(assoc);
    }
    // this.currentAssoc.parametre?.subscribe(rep=>console.log(rep.name))
  }
  gotoListeP() {
    this.navCont4.navigateForward('/liste-projet');
    console.log('this is the name : ');
  }
  gotoListeE() {
    this.navCont4.navigateForward('/liste-evenement');
    console.log('this is the name : ');
  }
  gotoListeN() {
    this.navCont4.navigateForward('/liste-notification');
    console.log('this is the name : ');
  }
  async alerte(donnee: any) {
    const alert = await this.alertController.create({
      header: 'log par Numero',

      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Opération annulée');
          },
        },
      ],
    });

    await alert.present();
  }
  // lecliks(dons:any) {
  //   this.navCont.navigateForward('/suivent',{state: {donne: dons,nam:this.name}})
  //   console.log('this is the name : ',dons.nom1)
  // }
}
