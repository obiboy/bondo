import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabPPage } from './tab-p.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabPPage,
    children :[
      {
        path: 'tab-accueil',
        loadChildren: () => import('../tab-accueil/tab-accueil.module').then( m => m.TabAccueilPageModule)
      },
      {
        path: 'tab-activite',
        loadChildren: () => import('../tab-activite/tab-activite.module').then( m => m.TabActivitePageModule)
      },
      {
        path: 'tab-comm',
        loadChildren: () => import('../tab-comm/tab-comm.module').then( m => m.TabCommPageModule)
      },
      {
        path: 'tab-autre',
        loadChildren: () => import('../tab-autre/tab-autre.module').then( m => m.TabAutrePageModule)
      },
    ]
  },
  {
    path: '',
    redirectTo:'tabs/tab-accueil',
    pathMatch: 'full'
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabPPageRoutingModule {}
