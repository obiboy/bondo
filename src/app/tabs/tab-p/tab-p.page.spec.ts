import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabPPage } from './tab-p.page';

describe('TabPPage', () => {
  let component: TabPPage;
  let fixture: ComponentFixture<TabPPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TabPPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
