import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabAutrePage } from './tab-autre.page';

describe('TabAutrePage', () => {
  let component: TabAutrePage;
  let fixture: ComponentFixture<TabAutrePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(TabAutrePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
