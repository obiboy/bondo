import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab-autre',
  templateUrl: './tab-autre.page.html',
  styleUrls: ['./tab-autre.page.scss'],
})
export class TabAutrePage implements OnInit {
  public alertButtons = ['Envoyer'];
  public alertInputs = [
    {
      type: 'textarea',
      placeholder: 'Entrez votre préoccupation',
    },
  ];
  isAlertOpen = false;
  alertButtons1 = ['Action'];

  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }

  constructor(public navCont: NavController) {}

  ngOnInit() {}
  goTo(choix: number) {
    switch (choix) {
      case 1:
        this.navCont.navigateForward('/edit-profile');
        console.log('Premier choix');
        break;
      case 2:
        this.navCont.navigateForward('/porte-monnaie');
        console.log('Deuxième choix');
        break;
      case 3:
        this.navCont.navigateForward('/liste-associations');
        console.log('Troisième choix');
        break;
      case 4:
        this.navCont.navigateForward('/mes-notification');
        console.log('Troisième choix');
        break;
      case 5:
        this.navCont.navigateForward('/condition');
        console.log('Troisième choix');
        break;
      case 6:
        this.navCont.navigateForward('/a-propos');
        console.log('Troisième choix');
        break;

      default:
        break;
    }
  }
  logOut() {}
}
