import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabAutrePage } from './tab-autre.page';

const routes: Routes = [
  {
    path: '',
    component: TabAutrePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabAutrePageRoutingModule {}
