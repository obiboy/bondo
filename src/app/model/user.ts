export interface ProfileUser {
    uid: string;
    email?: string;
    firstName?: string;
    lastName?: string;
    displayName?: string;
    dateOB?:string;
    gender?:string;
    phone?: string;
    address?: string;
    status?:string
    photoURL?: string;
    associations?:string[];
  }