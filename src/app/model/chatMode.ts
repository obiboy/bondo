import { ProfileUser } from "./user";

export interface Chat{
    id?: string;
    lastMessage?: string;
    lastMessageDate?: Date;
    userIds: string[];
    user:ProfileUser[];

}
export interface Message{
    text:string;
    senderId:string;
    sendDate:Date;
}