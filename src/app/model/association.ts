import { object } from "@angular/fire/database";
import { MembersModel } from "./membre";

export interface AssociationModel{
    aId: string;
    name : string ;
    description?:string;
    headquater?:string;
    tel?:string;
    email?:string;
    president? : string ;
    members ?:MembersModel [];
    imgUrl?:string;
    

}
// ceci est une branche de ma collection sur firestore 
