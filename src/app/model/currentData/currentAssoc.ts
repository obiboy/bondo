import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AssociationModel } from "../association";

@Injectable()
export class CurrentAssoc{
    parametre?: Observable<AssociationModel>;
    // parametre?: any;
}