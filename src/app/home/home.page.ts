import { Component } from '@angular/core';
import { NgControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { DaniComponent } from '../components/dani/dani.component';
import { ChatGroupService } from '../service/chat.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [NavController,]

})
export class HomePage {
  user :any
 
  options = {
    centeredSlides: true,
    slidesPerView: 1,
    spaceBetween: -60,
  };

  categories = {
    slidesPerView: 2.5,
  };
  constructor(public navCont :NavController,private chatService:ChatGroupService) {}
  lecliks() {
    // this.navCont.navigateForward('/testlo');
    this.navCont.navigateForward('/testelog');
    console.log('this is the name : ',)
  }



  /////////////////////////
  creatChat(){
    const sees=this.chatService.createChatGroup('GroupTeste')
    console.log(sees)
  }
  

}
