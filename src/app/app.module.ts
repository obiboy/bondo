import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getDatabase, provideDatabase } from '@angular/fire/database';
import { getStorage, provideStorage } from '@angular/fire/storage';
import { FirebaseService } from './service/firebaseService';   
import { CompAccueilComponent } from './components/comp-accueil/comp-accueil.component';
import { FormsModule } from '@angular/forms';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire/compat';
import { UserService } from './service/user.service';
import { CurrentAssoc } from './model/currentData/currentAssoc';
@NgModule({
  declarations: [AppComponent],
  imports: [
    FormsModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    provideFirebaseApp(() =>
      initializeApp(
        environment.firebaseConfig
        // {
    //     //   projectId: 'bado-7bb5e',
    //     //   appId: '1:328202099806:web:f9bde203dbd835306728cf',
    //     //   databaseURL: 'https://bado-7bb5e-default-rtdb.firebaseio.com',
    //     //   storageBucket: 'bado-7bb5e.appspot.com',
    //     //   //locationId: 'us-central',
    //     //   apiKey: 'AIzaSyB4XiwQ76ugIAChcjRUSYWZRnR_olJmiVo',
    //     //   authDomain: 'bado-7bb5e.firebaseapp.com',
    //     //   messagingSenderId: '328202099806',
    //     // }
      )
    ),
    provideAuth(() => getAuth()),
    provideFirestore(() => getFirestore()),
    provideDatabase(() => getDatabase()),
    provideStorage(() => getStorage()),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireModule

  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    UserService,
    CurrentAssoc
    // Fournir la configuration Firebase avec l'injection token
    // { provide: FIREBASE_CONFIG, useFacto   ry: provideFirebaseConfig },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}